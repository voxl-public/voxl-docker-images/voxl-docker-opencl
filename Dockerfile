FROM aarch64/ubuntu:xenial

WORKDIR /home
COPY modalai-opencl-dev_1.0-1.deb .
RUN apt-get update
RUN apt-get install -y vim strace libglib2.0-0 g++ build-essential cmake
RUN dpkg -i modalai-opencl-dev_1.0-1.deb

CMD ["/bin/bash"]
