#!/bin/bash

PACKAGE_FILE=modalai-opencl-dev_1.0-1.deb
if [ ! -f $PACKAGE_FILE ]; then
    echo "Error: Please download the package file $PACKAGE_FILE before proceeding."
    exit -1
fi

TARGET_DIR=/data/docker/opencl
adb shell mkdir -p $TARGET_DIR
adb push $PACKAGE_FILE $TARGET_DIR
adb push Dockerfile $TARGET_DIR
adb shell docker build -t modalai-opencl:v1.0 /data/docker/opencl
