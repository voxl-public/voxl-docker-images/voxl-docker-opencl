# voxl-docker-opencl

This is an example of how to use OpenCL in a 64-bit Docker container running on target.

Before starting make sure that Docker is installed and running properly on target. See
the [Docker development user guide](https://docs.modalai.com/docker-on-voxl/) for more information.

ADB is required for the build script to function properly. [adb setup instructions](https://docs.modalai.com/setup-adb/)

This also requires some EULA protected proprietary binary libraries from Qualcomm. These
can be accessed via the ModalAI [Developer Dashboard](https://developer.modalai.com/)
Protected Downloads page. Look for the 64-bit OpenCL Package named: modalai-opencl-dev_1.0-1.deb.
Download the package into the project directory before starting.

This project creates a Docker image that can be used for OpenCL development. It can be tested
using the hellocl example application located in the VOXL [applications example project](https://gitlab.com/voxl-public/apps-proc-examples).

To build the docker image use: `./build-image.sh`. This will use adb to push the
required files to the target board and build the docker image.

To test the docker image, adb or otherwise log into the target and follow these steps:
```bash
# cd /data/docker/opencl
# git clone https://gitlab.com/voxl-public/apps-proc-examples.git src
# docker run -it --rm --privileged -v /data/docker/opencl:/opt/modalai-opencl-dev modalai-opencl:v1.0
```

Once in the docker run these commands:
```bash
# cd /opt/modalai-opencl-dev/src/hellocl/
# mkdir build
# cd build
# cmake ..
# make
# ./hellocl
```

This is the output you should see:
> root@d9d52110fbf0:/opt/modalai-opencl-dev/src/hellocl# ./hellocl\
> QUALCOMM Adreno(TM)\
> OpenCL 2.0 QUALCOMM build: commit #f437276 changeid # Date: 10/24/18 Wed Local Branch:  Remote Branch:  Compiler E031.36.03.00\
> this should be three: 3
